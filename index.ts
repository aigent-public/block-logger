import {ConsoleLogger} from "./src/console-logger";
import {IOptions, LogstashLogger} from "./src/logstash-logger";

export function console() {
    return new ConsoleLogger();
}

export function logstash(options: IOptions) {
    return new LogstashLogger(options);
}
