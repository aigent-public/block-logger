'use strict';


/**
 *
 * options = {
 *   host
 *   port
 *   domain
 *   level
 *
 * }
 */

import {getLogger, configure, Logger, Configuration} from "log4js";
import {ILogger} from "./logger-interface";

export type IOptions = {
    port: number;
    host: string;
    level: string;
    domain: string;
};


export class LogstashLogger implements ILogger {

    options: Configuration;
    logger: Logger;

    public constructor(options: IOptions) {

        options.port = options.port || 5050;
        options.domain = (options.domain || '').toLowerCase();

        this.options = {
            appenders: {
                logstash: {
                    host: options.host,
                    port: options.port,
                    type: `log4js-logstash-tcp`,
                    fields: {
                        application: options.domain
                    },
                    layout: {
                        type: 'pattern',
                        pattern: '%m'
                    },
                    category: options.domain
                },
                console: {
                    type: 'stdout'
                }
            },
            categories: {
                default: {
                    appenders: ['logstash', 'console'],
                    level: options.level
                }
            }
        };

        configure(this.options);

        this.logger = getLogger(options.domain);
    }

    public debug(message, ...args): void {
        this._log('debug', message, ...args);
    }

    public error(message, ...args): void {
        this._log('error', message, ...args);
    }

    public info(message, ...args): void {
        this._log('info', message, ...args);
    }

    public warn(message, ...args): void {
        this._log('warn', message, ...args);
    }

    public fatal(message, ...args): void {
        this._log('fatal', message, ...args);
    }

    public log(message, ...args): void {
        this._log('info', message, ...args);
    }

    private _log(type, message, ...args): void {
        if (args) {
            this.logger[type](message, Object.assign({logMessage: message}, ...args));
        }
    }
}
