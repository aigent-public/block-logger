'use strict';

import {ILogger} from "./logger-interface";

export class ConsoleLogger implements ILogger {

    public debug(message, ...args: any[]) {
        console.debug(message, ...args);
    }

    public error(message, ...args: any[]) {
        console.error(message, ...args);
    }

    public info(message, ...args: any[]) {
        console.info(message, ...args);
    }

    public warn(message, ...args: any[]) {
        console.info(message, ...args);
    }

    public fatal(message, ...args: any[]) {
        console.info(message, ...args);
    }

    public log(message: string, ...args: any[]) {
        console.log(message, ...args);
    }
}
