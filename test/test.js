'use strict';

const process = require('process');
const os = require('os');

const Logger = require('../dist/index');

const options = {
    host: process.env.LOGSTASH_HOST || '127.0.0.1',
    port: process.env.LOGSTASH_PORT || 5050,
    domain: 'com.aigent.pipeline.block.' + os.hostname(),
    level: 'debug'
};

const logger = Logger.logstash(options);

let counter = 1;
setInterval(() => {
    console.log(`sending test counter ${counter}`);
    logger.debug('test', {counter: counter++});
}, 1000);
